# Brumos Linux Web Portal with NODE.js #

* Web server & client linux creato con node.js
* Target Platform -> Raspberry pi 3/4 - current -> pi3
* Target OS -> 32 bit Raspberry OS (raspbian) 

* Version 0.5

* GOAL : implement a full MERN stack in version 2.0

* CHANGELOG :

	.( 0.1 ) _ Node js + socket.io + express + chart.js
			 _ Mobile-first css styling

	.( 0.2 ) _ added HTTPS redirection, local log (worst way?), pdf-creator-node 
			 _ chart.js + plugin live streaming
			 _ chart.js dounught

	.( 0.3 ) _ added tcp server (require 'net')
			 _ BUG pdf-creator-node dependencies no longer supported for ARM 32bit -> PDF creation not implemented

	.( 0.4 ) _ fixed PDF creation ( just server-side, pdf with no screenshot )  : PDFKit npm  
			 _ fix client-side project dir tree with js modules 
			 _ added jQuery for settings-div dynamic functions 
			 _ ReBuild chart-js-livestreaming module, see chartjs-plugin-streaming v 2.0 (luxon - luxon adapter)

	.( 0.5 ) _ PDF creation server side + visualization
			 _ Fix new socket.io connection for setting-div jQuery reloading -> create a module for socket (client-side)
			 _ last fix js/css client-side gui structure
			 _ start modeling document-type mongoDB (select way to store history data); 

* TODO : 
				 
	.( 0.6 )_ added realm mongoDB connection (working on schemas, best way to implements ) (?client side APP?) (pensa implementazioni future) 

			_V Fix server-side dir tree - add javascript modules [ DEPENDENCY INJECTION ] [ SERVICES ]

			_ execution log system structured 
			 	-> sol = winston npm package
	
			_ create chart.js fields for history ( prendo spunto da financial con candele max&min )

	.( 0.7 )_ GPIO interactions


	
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies

	NPM packages 

	SERVER

		_ Express HTTPD : https://www.npmjs.com/package/express

		_ Socket.io : https://socket.io/

		_ N-api : importa addon in c che gestisce logica business (serial + data_controller) [ Portal_NapiAddon ] https://nodejs.org/api/n-api.html

		_ node IP : utilities per avere info su indirizzi https://github.com/indutny/node-ip


		(gpio) 

	CLIENT 

		_ chart.js streaming plugin : https://nagix.github.io/chartjs-plugin-streaming/

		_ chart.js : https://www.chartjs.org/

		_ moment.js : https://momentjs.com/
		

* Database configuration

	v 1.0 -> mongoDB implementation

		ATLAS free cloud db 
			https://www.mongodb.com/it-it/cloud/atlas

		npm - mongoDB Realm
			https://docs.mongodb.com/realm/
			https://www.npmjs.com/package/realm

* How to run tests
	sudo node ./test/testNameCanChange

* Deployment instructions
	? Deployment from 2.0 ?

### Contribution guidelines ###

* Writing tests

* Code review
* Other guidelines



