const Logger = require('../services/loggerService');

const Serial = require('../services/serialService');
const Db = require('../services/realmService');
const Manager = require('../services/dataManager');
const Alarm = require('../services/alarmService');

const Business = require('../services/dataService');

const TcpBrumosService = require('../services/TcpService');

const HttpBrumosService = require('../services/HttpService');
const SocketBrumosService = require('../services/socketIO');

const logger = new Logger();

const srvHttp = new HttpBrumosService(logger);
const serial = new Serial(logger);
const db = new Db(logger);
const alarm = new Alarm(logger);


const manager = new Manager(logger, serial, db);

const business = new Business(logger, manager, alarm);

const socket = new SocketBrumosService(srvHttp, business);

module.exports = {
    socket
}