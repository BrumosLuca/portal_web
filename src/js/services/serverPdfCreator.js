const PDFDocument = require('pdfkit');
const fs = require('fs');
const fs_prom = require('fs/promises');
const path = require('path');
const blobStream = require('blob-stream');

const pdf = {

  getDailyFileNames : async function(path, day){

    let OKfiles = [];
  
    try {

      const files = await fs_prom.readdir(path);

      for (const file of files){
        if (file.startsWith(day)) 
          OKfiles.push(file);
      }

    } catch (err) {
      console.error(err);
    }

    return OKfiles;
  },
  createLocal: function (cps, back, alarm, dateAsTitle){
    // Create a document
    const doc = new PDFDocument();

    let now = new Date();
    let month = now.getMonth() + 1;
    if(month<10) month = "0" + month;

  
    doc.pipe(fs.createWriteStream(path.join("reports/"+ now.getFullYear() + "/" + month + "/", dateAsTitle + ".pdf") )) ;
    doc
      .fontSize(25)
      .text(' CPS : ' + cps + '  BACK : ' + back + " ALARM : " + alarm +  '\n'  , 100, 100);
    doc.end();

    return doc;
  },
  createBrowser: function (){

    const doc = new PDFDocument();
    // pipe the document to a blob
    const stream = doc.pipe(blobStream());
    // add your content to the document here, as usual
    doc
    .fontSize(25)
    .text('Some text with an embedded font!', 100, 100);
    // get a blob when you're done
    doc.end();

    var url = 'failed';

    stream.on('finish', function() {

      // get a blob you can do whatever you like with
      const blob = stream.toBlob('application/pdf');
      // or get a blob URL for display in the browser
      url = stream.toBlobURL('application/pdf');
      //iframe.url = url 
    });


    return url;
  }

}

module.exports = pdf;
