const net = require('net');

class TcpBrumosService{

    clients = [];
    TcpServer;

    constructor(dataService){
        this.dataService = dataService;
        this.#create();
    }

    #create(){

        this.TcpServer = net.createServer();

        this.TcpServer.listen(TCPport, () => {
            console.log("##    TCP  Server ON [%d]    ##", TCPport );
        });
                
        this.TcpServer.on('connection', (socket) => {

            console.log('TCP --> new client ' + socket.address().address + " [ " + socket.address().port + " ]");
        
            this.clients.push(socket);
                
            socket.on('end', () => {
                console.log('client ' + socket.address().address + ' [ ' + socket.address().port + ' ]  ' + ' disconnected');
                clients.pop(socket);
            });
        }) 
    }

}

module.exports = TcpBrumosService;




