cpsArray = Array(600);
backArray = Array(600);
alarmArray = Array(600);
timeArray = Array(600);

function add(dataSerialBrumos){
    cpsArray.push(dataSerialBrumos.cps);
    backArray.push(dataSerialBrumos.back);
    alarmArray.push(dataSerialBrumos.al);
    timeArray.push(dataSerialBrumos._id); 
};

function getCpsMinute(){
    return { min : Math.min(cpsArray) , avg : Math.avg(cpsArray) , max : Math.max(cpsArray) }
};

function getBackMinute(){
    return { min : Math.min(backArray) , avg : Math.avg(backArray) , max : Math.max(backArray) }
};

function getAlarmMinute(){
    return { min : Math.min(alarmArray) , avg : Math.avg(alarmArray) , max : Math.max(alarmArray) }
};


class DataManager{

    #dbCounter = 0;

    constructor(loggerService, serialService, dbService){
        this.logger = loggerService;
        this.serial = serialService;
        this.database = dbService;
    }

    readSerialData(){
        
        let x = this.serial.getData();

        if(x != null)
        {
            add(x);
            this.#count4Db();
        }
        
        return x;
     
    }

    getData(){
        return { timeArray, cpsArray, backArray, alarmArray };
    }

    changeSigma(newSigma){
        this.database.EditSettings( { sigma : newSigma } );
        this.serial.changeSigma(sigma);
    }

    #count4Db(){

        this.#dbCounter = this.#dbCounter++;

        if(this.#dbCounter == 600){
            this.#dbCounter = 0;
            this.#pushLast10Min2Db();
        }
    }

    #pushLast10Min2Db(){
        this.database.createData({ back : getBackMinute(), cps : getCpsMinute() , alarm : getAlarmMinute() });
    }

}

module.exports = DataManager;