const serialAddon = require('/home/luca/Scrivania/NodeJS/2_NapiAddon/build/Release/addon');

class SerialService{

    constructor(logger){
        this.logger = logger;
        this.open();
    }

    open(){

        if( serialAddon.openDeviceInterface() < 1 ){
            console.log("\nOPEN USB DEV FAILED\n" );
            throw err;
        }else{
            console.log("# OK # - USB device open ['ttyUSB0']");
        }
    }

    getData(){

        let values = serialAddon.getData();
     
        //check better for data integity?
        if((values == undefined || values.length == 0)) return null;

        const data = { cps : values[0], back : values[1], al : values[2], _id : new Date().getTime(), A : values[3] }

        return data;
    }


    changeSigma(sigma){
        serialAddon.changeSigma(+sigma);
    }

}

module.exports = SerialService;
