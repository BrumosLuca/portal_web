
const index = require("./index");
const brumos_id = require("./brumos").getID;


exports.getDataHour = async (startTime, endTime) => {
}

exports.getData12Hour = async (startTime, endTime) => {
}

exports.getDataDay = async (startTime, endTime) => {
}

exports.getDataWeek = async (startTime, endTime) => {
}

exports.getDataMonth = async (startTime, endTime) => {
}

exports.getData10Min = async (startTime, endTime) => {

    const realm = index.getRealm();

    try{
        const query = realm.objects("Data"); 

        if(query != undefined) const result = query.filtered("time>=" + startTime + "&& time <="+ endTime);

        console.log(result);

        return result;

    }catch(err){
        console.error("error get10MinData");
    }
}

/* myData
* { back : { max, avg, min }, cps : { max, avg, min } , alarm : { max, avg, min } }
*/
exports.pushData10Min = async (myData) =>{
    const realm = index.getRealm();

    let tenMin;

    realm.write ( () => {
        tenMin = realm.create("Data", {
            time: new Date(),
            brumos_app: brumos_id(),
            cps:myData.cps,
            back:myData.back,
            alarm:myData.alarm
        })

    })

    console.log(tenMin);

}

/*
async function push10Min2Realm(myData){
        
    try {

            const realm = await realmMongo.open({
                schema: [realmModels.Data],
                schemaVersion:3
            });

            let lastHour;
            // Open a transaction.
            realm.write(() => {
                // Assign a newly-created instance to the variable.
                lastHour = realm.create("Data", { 
                    time:  Date(), 
                    brumos_app: IDENTIFIER,
                    cps: myData.cps,
                    back: myData.back,
                    alarm: myData.alarm,
                   
                });
            });

            realm.close();

            console.log(lastHour);

        } catch (err) {
            console.error("Failed to open the realm", err.message);
        }
}
*/