const realmMongo = require("realm");
const schemas = require("./schemas");

let realm;

async function getRealm() {

  if (realm == undefined) {
    realm = openRealm();
  }

  return realm;
}


async function openRealm() {
    const config = {
      schema: [schemas.BrumosSchema , schemas.DataSchema , schemas.ReportSchema]
    };

    return realmMongo.open(config);
}


async function closeRealm(partitionKey) {
    if (realm != undefined) {
        realm.close();
        realm = undefined;
    }
}

exports.getRealm = getRealm;
exports.closeRealm = closeRealm;




function querySettings(){
    let main = queryBrumos().then( console.log(main) );
    return main.settings;
}

async function queryBrumos(){

    try {

        const realm = await realmMongo.open({
            schema: [realmModels.Brumos],
            schemaVersion:1
        });

        const brumos = realm.objectForPrimaryKey("_id", "testName"); 
        
        realm.close();

        console.log("settings query : " + brumos.settings );

        return brumos;

    } catch (err) {
        console.error("Failed to open the realm", err.message);
    }

}

async function queryData(date){

    try{

        const realm = await realmMongo.open({
            schema: [realmModels.Data],
            schemaVersion:2
        });

        // query
        const set = realm.objectForPrimaryKey("settings", 1); // search for a realm object with a primary key that is an int.
        
        realm.close();

        console.log("settings query : " + set);

        return set;


    }catch(err) {

    }

}


async function pushSettings2Realm(sigmaNew){

    try {

        const realm = await realmMongo.open({
            schema: [realmModels.Settings],
            schemaVersion:2
        });
        
        realm.write(() => {
            const set1 = realm.objects("Settings")[0]; // search for a realm object with a primary key that is an int.
            set1.sigma = parseInt(sigmaNew);

            console.log(set1)
        })

        realm.close();

    } catch (err) {
        console.error("Failed to open the realm", err.message);
    }
}
