const index = require("./index");

exports.getID = async () => {
    return this.getBrumos()._id;
}

exports.getBrumos = async () => {

    const realm = index.getRealm();

    const br = realm.objects("Brumos")[0];

    return br;
}

exports.createBrumos = async (companyName) => {

    const realm = index.getRealm();
    let br;

    realm.write( () => {

        br = realm.create("Brumos", 
            {
                _id : companyName,
                infoSW : "version 0.6",
                infoHW : "nessuna"      
            });    
    });

    return br;
}

exports.editSettings = async (newSettings) => {

    /* newSettings Format : 
        detector  : {
            id : "int?",
            nome : "string?"
        },
        sigma : "int",
        durataMisura:"int?",
        sigmaMisura:"int?"
    *
    */

    const br = this.getBrumos();

    br.settings = newSettings;
}

/*
async function getBrumos(){

    try{
        const realm = await realmMongo.open({
            schema: [realmModels.Brumos],
            schemaVersion:2
        });

        const brumos = realm.objects("Brumos"); 

        realm.close();

        if(brumos.length == 1) IDENTIFIER = brumos[0];
        else{

            console.log(" ERRORE realm brumos_object : ", + brumos + "\nlenght : " + brumos.length )

            console.log(" if undefined maybe db was not initialized, trying to init ");

            if(brumos == undefined){
                initAutomaticBrumosSchema();
            }

        }
    }catch(err){
        console.log(err);
    } 
}

async function initAutomaticBrumosSchema(){

    const realm = await realmMongo.open({
        schema: [realmModels.Brumos],
        schemaVersion:1
    });

    let foo = realm.create("Brumos", {

        _id : "testName",
        infoSW: "versione_0.6",
        infoHW: "versione_Alpha",
        settings: {
            detector : {
                _id:1,
                info: "unico"
            },
            sigma : 5
        }

    });

    console.log(foo._id);
}*/