const Brumos = {
    name:"Brumos",
    properties : {
        _id:"string",
        infoSW:"string",
        infoHW:"string",
        settings: "Settings?"
    },
    primaryKey: "_id"
}

const Settings = {
    name : "Settings",
    embedded : true,
    properties : {
        detector  : {
            id : "int?",
            nome : "string?"
        },
        sigma : "int",
        durataMisura:"int?",
        sigmaMisura:"int?"
    }
}

const Data = {
    name: "Data",
    properties: {
      time: "date",
      brumos_app:"string",  
      cps: {
          max : "int",
          avg : "int",
          min : "int"
      },
      back: {
          max : "int",
          avg : "int",
          min : "int"
      },
      alarm: {
          max : "int",
          avg : "int",
          min : "int"
      },
    },
    primaryKey: "time",
}

const Report = {
    name: "Report",
    properties: {
      brumos_app:"string",  
      start_time:"date",
      end_time:"date",
      data:{
          cps:"int[]",
          back:"int[]",
          alarm:"int[]"
      },
      type:"string",
      note:"string"
    }
}


exports.BrumosSchema = Brumos;
exports.DataSchema = Data;
exports.ReportSchema = Report;