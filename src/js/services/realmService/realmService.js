const Brumos = require("./brumos");
const Data = require("./datas");
const Report = require("./reports");

class RealmService{

    constructor(logger){
        this.logger = logger;
    } 

    createBrumos(companyName){
        Brumos.createBrumos(companyName);
    } 

    createData(myData){
        Data.pushData10Min(myData);
    }

    creteReport(newR){
        Report.creteReport(newR);
    }

    getBrumos(){
        return Brumos.getBrumos();
    } 

    getData(startTime, endTime, resolution){

        switch(resolution){
            case "minute" : return Data.getData10Min(startTime,endTime);
            case "hour" : return Data.getDataHour(startTime,endTime);
            case "12hour" : return Data.getData12Hour(startTime,endTime);
            case "day" : return Data.getDataDay(startTime,endTime);
            case "week" : return Data.getDataWeek(startTime,endTime);
            case "month" : return Data.getDataMonth(startTime,endTime);
            default : return Data.getData10Min(startTime,endTime);
        }

    }

    getLastReport(){
        return Report.getLastReport();
    }

    getReports(startTime, endTime){ 
        return Report.getReport(startTime,endTime);
    }

    editSettings(newSettings){
        Brumos.editSettings(newSettings)
    }
}

module.exports = RealmService;