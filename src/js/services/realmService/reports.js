const index = require("./index");
const brumos_id = require("./brumos").getID;

exports.getLastReport = async () => {
    const realm = index.getRealm();
    const lastReport = realm.object("Reports")[0];
    return lastReport;
}

exports.getReport = async (start, end) => {
    const realm = index.getRealm();

    const result = realm.object("Report").filtered("start_time <= $start && end_time > $end");

    return result;
}

exports.creteReport = async (report_info) => {

    const realm = index.getRealm();

    let r;

    realm.write ( () => {
        r = realm.create("Report", {
            start_time : report_info.start,
            end_time: report_info.end,
            brumos_app: brumos_id(),
            cps:report_info.cps,
            back:report_info.back,
            alarm:report_info.alarm,
            type:"Alarm",
            note:"..."
        })

    })

    console.log(r);
}

exports.EditReport = async () => {

}