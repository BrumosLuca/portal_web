class SocketBrumosService{

    io;
    
    constructor(httpService, dataService){
        this.httpService = httpService;
        this.dataService = dataService;
        this.#create();
    }
    
    start(){
       this.executeServer();
    }

    execute(){
        this.#processData(this.dataService.readData())
    }

    #processData(data){
        this.io.emit('update', data);
    }

    #create(){
        this.io = require('socket.io')(this.httpService.HTTPserver);
        this.#socketMessageExchange();
    }

    #socketMessageExchange(){

        this.io.on('connection', (socket) => {
            
            console.log('\n HTTP + --> new client ' + socket.request.connection.remoteAddress);

            this.httpService.addClient(socket);

            socket.on('ready', () => {
                let y =  this.dataService.getFullData();
                socket.emit('dataOnConnection', y );
            });

            socket.on('ReportsReqDate', (year,month,day) => {

                console.log("ask for reports of " + year + "-" + month + "-" + day);  

                let value = this.dataService.getReports(date); 
                //myPdf.getDailyFileNames(path.join(__dirname, "reports/" + year + "/" + month), day).then((value) => 

                //getDataFromDB
            
                socket.emit('ReportsResDate',  value);

            });

            socket.on('SigmaChanged', (sig) => {
                this.dataService.changeSigma(sig);
            });

            socket.on("disconnect", (reason) => {
                console.log("--> - client disconnected -- ID : " + socket.id + " reason : " + reason);
                this.httpService.removeClient(socket);
            });
        
        //realmTesing FF
            socket.on('alarmTestFromClt', () => {
                var testAl = {     
                    cps: 500,
                    back: 300,
                    alarm: 450,
                    _id: Date.now() 
                };

                this.dataService.alarmService.stepAlarm(testAl);
            });

        });

    }

}

module.exports = SocketBrumosService;

/*
function BRUMOS_SERVER () {

    let data = serialBrumos.getData();

    if( data != null ){

        if(srvHTTP.clients.length > 0)
            io.emit('update', data);

        if(srvTCP.clients.length > 0)
            srvTCP.clients.forEach(clientSocket => {
                clientSocket.write("D/" + data.cps + "/" + data.back + "/" + data.al + "/" + data.A + "/" + data._id);
        });

        if(data.A || inAlarm){
            
            let a = Alarm(data); 

            switch(a){ 
                case -1 : io.emit('StopAlarm', maxAlarmValue); 
                    break;
                case 0 : // no alarm 
                    break;
                case 1 : io.emit('StartAlarm');
                    break;
                case 2 : console.log("Update alarm MAX");
                    break;
            }

        };

                
        dataBrumos.actions.pushToHistory(data);

    }
      
    setTimeout(BRUMOS_SERVER, 1000); 
};
*/

        