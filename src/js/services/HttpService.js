const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const https = require('https');
const http = require('http');

class HttpBrumosService{

    clients = [];
    HTTPport = 443;
    HTTPserver;

    constructor(logger){
        this.logger = logger;
        this.#create();
    }

    addClient(ipAddr){
        return this.clients.push(ipAddr); 
    }
    
    removeClient(ipAddr){
        let index = this.clients.indexOf(ipAddr);
        if(index > -1) return this.clients.splice(ipAddr,1);
    }

    #create(){
        // Routing Path to root server dir 
        app.use(express.static(path.join(__dirname, '../../../public')));
        app.use(express.static(path.join(__dirname, '../../../reports')));

        this.HTTPserver = https.createServer({
                    key: fs.readFileSync(path.join(__dirname, '../../../cert/server.key')),
                    cert: fs.readFileSync(path.join(__dirname, '../../../cert/server.cert'))
        }, app)
        
        this.HTTPserver.listen(this.HTTPport, () => {
            console.log("# OK # - HTTP Server ON  [%d] ", this.HTTPport );
        });

        http.createServer(function (req, res) {
            res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
            res.end();
        }).listen(80);
    }
}


module.exports = HttpBrumosService;






