class AlarmService{

    constructor(logger){
        this.logger = logger;
        this.alarmCounter = 0;
        this.maxAlarmValue = 0;
        this.inAlarm = false;
    }

    getStatus(){
        return this.inAlarm;
    }

    getMaxValue(){
        return this.maxAlarmValue;
    }

    stepAlarm(dataSerialBrumos){

        if(!this.inAlarm){
            this.#start();
            return 1;
        }

        if(this.inAlarm && dataSerialBrumos.cps > this.maxAlarmValue ){
            this.#editMax(dataSerialBrumos.cps);
            return 2;
        }
        
        if(this.alarmCounter == 0){
            this.#end();
            return -1;
        }
    
        if(alarmCounter>0) alarmCounter--;
    
        return 0;

    }

    #start(){
        this.alarmCounter = 10;
        this.maxAlarmValue = dataSerialBrumos.cps;
        this.inAlarm = true;
    }

    #editMax(cpsVal){
        if(alarmCounter<5) alarmCounter = 5;
        maxAlarmValue = cpsVal;
    }
    
    #end(){

        this.inAlarm = false;
        
        //now era utilizzato come path per la roba salvata mi sa che non serve più a nulla
        /*
        var now = new Date().toLocaleDateString("it-IT", { 
            month: "2-digit",
            day: "2-digit",
            hour:"2-digit",
            minute : "2-digit",
            second: "2-digit"
        });
        
        now = now.replace(",","");
        now = now.replace("/","-");

        console.log(now);
        */
    }
}


module.exports = AlarmService;
