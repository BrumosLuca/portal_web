class DataService{

    constructor(logger, manager, alarm ){
        this.logger = logger;
        this.dataManager = manager;
        this.alarmService = alarm;
    }

    readData(){
       return this.dataManager.readSerialData();
    }

    getFullData(){
        return this.dataManager.getData();
    }

    changeSigma(sigma){
        this.dataManager.changeSigma(sigma);
    } 
    
}

module.exports = DataService;
