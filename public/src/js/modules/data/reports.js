import { socket } from "../mySocket.js";

function dateReq(year, month , day){
    socket.emit('ReportsReqDate', year , month, day);
}

socket.on('ReportsResDate', (datesArray) => {
    fillReportList(datesArray);
});

const fillReportList = (datesArray) => {

    if( datesArray == undefined || datesArray.length == 0 ){

        var li = document.createElement("li");
        li.textContent = " Nessun report d'allarme per la data selezionata ";

        $('#dailyList').append(li); 

        return;
    }

    $('#dailyList').empty();

    datesArray.forEach(date => {
          
        var li = document.createElement("li");

        li.textContent = date;

        li.addEventListener("click", () => {

            console.log("asking for file : " + date);
            
            $('#embedPdf').show();

            $("#embedPdf").attr("src", "2021/10/" + date);

            $('#rhDiv').scrollTop();
        }

        );

        $('#dailyList').append(li);

    });

    $('#titleList').append(datesArray[0].substring(0,5));
}

export function askForDate(){

    var date = new Date($('#dsd1').val());

    if(isNaN(date.getTime()) )
    {
       fillReportList(undefined) ;
       return;
    }

    console.log(date); 

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear(); 

    console.log("should not print if date is not selected : " +  year + "-" + month + "-" + day);

    dateReq(year,month,day);

}
