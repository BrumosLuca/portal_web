import * as reports from "./reports.js";

$('#measureTab').click( () => {

    selectTab('#measureTab');
    unselectTab('#reportTab');
    unselectTab('#historyTab');


    $('#rhDiv').load('../src/html/data/measureSelection.html');
});

$('#reportTab').click( () => {

    unselectTab('#measureTab');
    selectTab('#reportTab');
    unselectTab('#historyTab');

    $('#rhDiv').load(('../src/html/data/reportsSelection.html'), () => {
        reports.askForDate();
    });
});

$('#historyTab').click( () => {

    unselectTab('#measureTab');
    unselectTab('#reportTab');
    selectTab('#historyTab');

    $('#rhDiv').load('../src/html/data/measureSelection.html');
});

function selectTab(id4jQ){
    $(id4jQ).css('font-weigth','bolder');
    $(id4jQ).css('background-color','#2e9fea');
}

function unselectTab(id4jQ){
    $(id4jQ).css('font-weigth','bold');
    $(id4jQ).css('background-color','transparent');
}