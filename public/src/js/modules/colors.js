var chartColors = {
    red: 'rgb(252, 56, 56)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(117, 255, 120)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)',
    white: 'rgb(255,255,255)',
    lightBlue : 'rgba(54, 162, 235, 0.5)',
    lightRed : 'rgba(252, 56, 56, 0.5)',
    lightGreen : 'rgba(117, 255, 120, 0.5)',
    superLightBlue : 'rgba(54, 162, 235, 0.3)',
    superLightRed : 'rgba(252, 56, 56, 0.3)',
    superLightGreen : 'rgba(117, 255, 120, 0.3)',
    
};

export {chartColors as colors};