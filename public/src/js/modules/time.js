function showTime(){

    var result = new Date().toLocaleString('it-IT', {weekday: 'long', month: '2-digit', day: '2-digit', hour : '2-digit' , minute : '2-digit' , second : '2-digit'});
    // format as localTime

    document.getElementById('digitalClock').textContent = result;

    setTimeout(showTime, 1000);
}

showTime();
  