// breakpoint 992px
// media query listener DEPRECATED 

var mql = window.matchMedia('(max-width: 992px)');

function smallScreen(){

    window.myChart.config.options.legend.display = false;

    window.myChart.config.data.datasets[0].borderWidth = 0;
    window.myChart.config.data.datasets[0].pointRadius = 0;

    window.myChart.config.data.datasets[1].borderWidth = 2;
    window.myChart.config.data.datasets[1].pointRadius = 0;

    window.myChart.config.data.datasets[2].pointRadius = 0;
    window.myChart.config.data.datasets[2].borderWidth = 0;

    window.myChart.update(); 
}

function bigScreen(){

    window.myChart.config.options.legend.display = true;

    window.myChart.config.data.datasets[0].borderWidth = 0;
    window.myChart.config.data.datasets[0].pointRadius = 0;

    window.myChart.config.data.datasets[1].borderWidth = 3;
    window.myChart.config.data.datasets[1].pointRadius = 2;
    
    window.myChart.config.data.datasets[2].pointRadius = 0;
    window.myChart.config.data.datasets[2].borderWidth = 0;

    window.myChart.update(); 
}

mql.addEventListener("change", (e) => {

    if (e.matches) {
      smallScreen();
  } else {
      bigScreen();
  }

})