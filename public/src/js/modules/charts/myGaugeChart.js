var configCps = {
    type: 'doughnut',
    data: {
      datasets: [{
        data: [10,10],
        borderWidth: 1
      }]
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display:false,
        },
        title: {
          display: true,
          text:''
        }
      }
    },
};

var configBack = {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [10,10],
      borderWidth: 1
    }]
  },
  options: {
    responsive: true,
    plugins: {
      legend: {
        display:false,
      },
      title: {
        display: true,
        text:''
      }
    }
  },
};

var configAl = {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [10,10],
      borderWidth: 1
    }]
  },
  options: {
    responsive: true,
    plugins: {
      legend: {
        display:false,
      },
      title: {
        display: true,
        text:''
      }
    }
  },
};


var MAX = 0;

function editMax(value){

  var newMax =  value * 1.5 ;

  if(newMax > MAX){
    MAX = newMax;
  }

}

const actions = {
    initBack : function(gauge, color1, color2){

      configBack.data.datasets[0].label = 'BACKGROUND';
      configBack.data.datasets[0].backgroundColor = [ color1 , 'rgb(255,255,255)' ];
      configBack.data.datasets[0].borderColor = [ color2 ,  color2 ];
      gauge = new Chart(gauge,configBack);
      return gauge;

    },
    initCps : function(gauge, color1, color2){

      configCps.data.datasets[0].label = 'CPS';
      configCps.data.datasets[0].backgroundColor = [ color1 , 'rgb(255,255,255)' ];
      configCps.data.datasets[0].borderColor = [ color2 ,  color2 ];
      gauge = new Chart(gauge,configCps);
      return gauge;

    },
    initAl : function(gauge, color1, color2){

      configAl.data.datasets[0].label = 'ALARM';
      configAl.data.datasets[0].backgroundColor = [ color1 , 'rgb(255,255,255)' ];
      configAl.data.datasets[0].borderColor = [ color2 ,  color2 ];
      gauge = new Chart(gauge,configAl);
      return gauge;

    },
    refresh : function(gauge, value, textElement ) {

      // update del max può essere solo aggiornato sull' al value (è il maggiore )
      editMax(value);
      gauge.data.datasets[0].data = [value, MAX-value];
      textElement.textContent = value;
      gauge.update();

    }
}

export {actions};