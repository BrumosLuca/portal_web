import { colors } from "../colors.js";

var config = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
              label: 'BACK',
              backgroundColor: colors.superLightGreen,
              borderColor: colors.green,
              fill: true,
              spanGaps:true,
              showLine:true,
              cubicInterpolationMode : 'default',
              pointStyle : 'none',
              borderWidth : 0,
              pointRadius: 0,
              borderDash: [4, 2],
              data: []
        }, 
        {
            label: 'CPS',
            backgroundColor: colors.lightBlue,
            borderColor: colors.blue,
            borderWidth: 3,
            pointRadius: 1,
            spanGaps:true,
            showLine:true,
            cubicInterpolationMode : 'monotone',
            fill : false,
            data: []
        }
        , 
        {
            label: 'ALARM',
            backgroundColor: colors.lightRed,
            borderColor: colors.red,
            borderWidth: 0,
            pointRadius: 0,
            borderDash: [ 5 , 2 ],
            fill: 'end',
            spanGaps:true,
            showLine:true,
            cubicInterpolationMode : 'default',
            data: []
    }]
    },
    options: {
        legend: {
              display: false,
              position: 'top',
        },
        title: {
            text : "BRUMOS WEB PORTAL",
            display: false,
        },
        scales: {
            x: {
                type: 'realtime',
                realtime: {
                    duration: 60000,
                    refresh: 1000,
                    delay: 2000,
                }
            },
            y: {
                title :{
                    display:true,
                    text:'CPS'
                },
                min : 0,
                max : 500,
                ticks:{
                    stepSize : 50
                }
            }
        },
        tooltips: {
            enabled : false
        }
    }
};

function editMax(chart, newValCps){

    let newMax = newValCps * 2 + ( 50 - (newValCps*2)%50  ) ; 

    if(newMax > chart.config.options.scales.y.max){
        chart.config.options.scales.y.max = newMax;
    }
} 

const actions = {
    
    init : function(chart){
        chart = new Chart(chart,config);
        return chart; 
    },

    fill : function(chart, timeArray, backArray, cpsArray, alarmArray ){

        chart.config.data.labels = timeArray;

        chart.data.datasets[0].data = backArray;

        chart.data.datasets[1].data = cpsArray;

        chart.data.datasets[2].data = alarmArray;
        
        chart.update('quiet');
    },

    refresh : function(chart, alarm, cps, back, time) {

        chart.config.data.labels.push(time);

        chart.data.datasets[0].data.push(back); 
        chart.data.datasets[1].data.push(cps); 
        chart.data.datasets[2].data.push(alarm); 

        editMax(chart,cps);
       
        chart.update('quiet');
        
    },
}

export {actions};