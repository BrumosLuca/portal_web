import { colors } from '../colors.js';
import { actions as cartesian } from './myCartesianChart.js';
import { actions as doughnut } from './myGaugeChart.js';


const actions = {

    init : function() {

        window.myChart = cartesian.init(document.getElementById('myChart').getContext('2d')) ;

        window.backGauge = doughnut.initBack(document.getElementById('backGauge'), colors.lightGreen , colors.green );
        
        window.cpsGauge = doughnut.initCps(document.getElementById('cpsGauge'), colors.lightBlue , colors.blue );
    
        window.alarmGauge = doughnut.initAl(document.getElementById('alarmGauge'), colors.lightRed, colors.red); 
    },

    fillOnStart : function(timeArray, backArray, cpsArray, alarmArray) {
        cartesian.fill(window.myChart,timeArray,backArray,cpsArray,alarmArray);
    },

    refreshUpdate : function(data){

        let cps = data.cps;
        let back = data.back;
        let al = data.al;

        cartesian.refresh(window.myChart,al,cps,back, data._id);

        doughnut.refresh(window.cpsGauge,cps,document.getElementById("cps"));
        doughnut.refresh(window.backGauge,back,document.getElementById("back"));
        doughnut.refresh(window.alarmGauge,al,document.getElementById('alarm'));

    },

    savePngScreenshot : () =>{

        let img = window.myChart.toBase64Image();
        
        var a = document.createElement('a');
        a.href = img;
        a.download = 'screenshot.png';

        
        a.click();

        return 'ok';
    }

}


export { actions }; 