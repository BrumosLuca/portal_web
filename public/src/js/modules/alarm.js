var alarm = {

    startRoutine : function(){
        document.getElementById('titleDiv').style.backgroundColor = "rgba(" + 252 + "," + 56 + "," + 56 + "," + 0.75 + ")"; 
    },

    stopRoutine : function () {
        document.getElementById('titleDiv').style.backgroundColor = "rgba(" + 54 + "," + 162 + "," + 235 + "," + 0.1 + ")";
    }
}

export {alarm};