import './modules/time.js';

import './modules/settings/settings.js';
import './modules/data/dataMain.js';

import { actions as AllCharts } from './modules/charts/myChartsMain.js';
import { alarm } from './modules/alarm.js';
import { socket } from './modules/mySocket.js';

console.log("default path io : " + window.location);

document.getElementById('testButton').addEventListener('click', () => {
    socket.emit('alarmTestFromClt');
});

window.onload = function() {
    AllCharts.init();
    socket.emit('ready');
};

socket.on('dataOnConnection', (myData)  => {
   AllCharts.fillOnStart(myData.timeArray, myData.backArray, myData.cpsArray, myData.alarmArray);
});

socket.on('update', (my_data) => {
    //console.log(my_data);
    AllCharts.refreshUpdate(my_data);
});

socket.on('StartAlarm', () => {
    alarm.startRoutine();
});

socket.on('StopAlarm' , (maxAlval) => {
   // AllCharts.savePngScreenshot();
    alarm.stopRoutine();
});


  


